import { Component, OnInit } from '@angular/core';
import { tile} from 'src/app/model/tile';


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {

  // property
  newTile: tile = {
    id: 0,
    letter:''
  };

  constructor() { }

  ngOnInit(): void {
  }

}
